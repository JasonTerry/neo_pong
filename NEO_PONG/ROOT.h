// MASS INCLUDE FOR SFML FUNCTIONALITY IN THE ROOT
// 

#include	<SFML/Window.hpp>		// SFML Window Header
#include	<SFML/Graphics.hpp>		// SFML Graphics Header
#include	<SFML/System.hpp>		// SFML System Header
#include	<SFML/Audio.hpp>		// SFML Audio Header
#include	<SFML/Main.hpp>			// SFML Main Header
#include	<iostream>				// IOSTREAM for in / out maybe?
#include	<math.h>				// Math.h for number sutff.
#include	<vector>				// Vectors
#include	<string>				// String ?? Not sure if needed.

// Sets up inital global consts for various things.

// Window Dimensions?
const int winWIDTH;
const int winHEIGHT;

// FPS Limit
const int FPS_LOCK;
