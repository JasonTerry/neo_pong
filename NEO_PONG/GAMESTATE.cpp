// Handles the changing and executing of various game-states

/* ...

Game States---
    - InitState :: Loads inital game data, renders splash and transfers to menu screen. Checks for DLL's etc.
        - Main Menu :: Renders main menu images and sound, very basic.
            - Options Menu :: Renders options menu, can revert to main menu state. ??? Options stored in config ???
            - Multiplay Menu :: Opens multiplay menu for direct IP connection ??? Port stuff ???
            - Soloplay Menu :: Soloplay Menu sets up the config for a new game instance.
        - Initplaydata :: Runs through setting up a new game instance, and starts the gameplay loop.
            - Game is running :: Runs the gameplay loop until broken via PUASE, GAMEOV, or QUIT events?
            - Score Menu :: Displays list of scores from previous play sessions.   

... */